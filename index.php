<?php

    session_start();

    include_once("connection.php");
    include_once("controllers/base.php");
    include_once("models/main.php");

    $controller_path = "controllers/";
    $controller_suffixe = "Controller";

    // On récupère le bon controller
    if(isset($_GET['page']) && $_GET['page'] != "")
    {
        // Charge le controller
        require_once($controller_path.$_GET['page'].".php");

        $controller_name = ucfirst($_GET['page'].$controller_suffixe);

        $c = new $controller_name();

        // Exécute la méthode demandée ou index() par défaut
        if(isset($_GET['action']) && $_GET['action'] != "")
        {
            $c->{$_GET['action']}();
        }
        else
        {
            $c->index();
        }
    }



?>