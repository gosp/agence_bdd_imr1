INSERT INTO `personne` (`idpersonne`, `nom`, `prenom`) VALUES
(1, 'Willis', 'Bruce'),
(2, 'Freeman', 'Martin'),
(3, 'Freeman', 'Morgan'),
(4, 'Kylmer', 'Val'),
(5, 'Jovovitch', 'Mila'),
(6, 'Cage', 'Nicolas'),
(7, 'Cumberbatch', 'Dominic'),
(8, 'Rodriguez', 'Michele');

INSERT INTO `residence` (`idresidence`, `nom`) VALUES
(1, 'Les rosiers'),
(2, 'Le chene vert'),
(3, 'Le grand bleu'),
(4, 'Auguste Thos'),
(5, 'Kercleguer');

INSERT INTO `type` (`idtype`, `nom`, `loyer`) VALUES
(1, 'T1', 150),
(2, 'T2', 225),
(3, 'T3', 330),
(4, 'T4', 475),
(5, 'F1', 100),
(6, 'F2', 170),
(7, 'F3', 260),
(8, 'F4', 315);

INSERT INTO `appartement` (`idappartement`, `idtype`, `idresidence`) VALUES
(1, 1, 2),
(2, 2, 1),
(3, 4, 5),
(4, 7, 3),
(5, 1, 1),
(6, 1, 1),
(7, 2, 3),
(8, 3, 3),
(9, 7, 2),
(10, 5, 2),
(11, 7, 3),
(12, 6, 3),
(13, 3, 4),
(14, 7, 4),
(15, 1, 5),
(16, 1, 5),
(17, 3, 5),
(18, 4, 5),
(19, 5, 5),
(20, 6, 5),
(21, 7, 5),
(22, 8, 5);

INSERT INTO `demande` (`idpersonne`, `idtype`, `idresidence`, `adresse`, `telephone`, `date`) VALUES
(2, 2, 3, '15 rue des charmes dorés', '0666666666', '2014-07-17'),
(1, 6, 4, '06 le rhun', '0677777777', '2014-07-24'),
(2, 1, 4, '15 rue des rois mages', '0633333333', '2014-09-10'),
(4, 7, 5, '17 rue du diable', '0688888888', '2014-09-11'),
(5, 2, 2, '18 rue des sentinelles', '0699999999', '2014-11-13'),
(7, 6, 5, '06 rue du soleil', '0611111111', '2014-09-17');

INSERT INTO `locataire` (`idlocataire`, `date_entree`, `idappartement`) VALUES
(1, '2014-06-24', 21),
(2, '2014-06-25', 5),
(3, '2014-06-30', 7),
(4, '2014-07-24', 6),
(6, '2014-06-02', 20),
(8, '2014-05-05', 15);