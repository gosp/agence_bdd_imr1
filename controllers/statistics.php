<?php
	
	class StatisticsController extends BaseController{

		public function __construct()
		{
			parent::__construct();
		}

		public function index()
		{
			if($this->isConnected())
			{
				$data['occupation_globale'] = $this->model->get_global_occupation_statistics();
				$data['occupation_par_residence'] = $this->model->get_each_residence_occupation_statistics();
				$this->render("statistiques", $data);
			}
			else
			{
				$this->redirect("page=connection");
			}
		}
	}
?>