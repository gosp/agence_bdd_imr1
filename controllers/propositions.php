<?php
	
	class PropositionsController extends BaseController{

		public function __construct()
		{
			parent::__construct();
		}

		public function index()
		{
			if($this->isConnected())
			{
				$data['propositions_meme_type'] = $_SESSION['propositions_meme_type'];
				$data['propositions_meme_residence'] = $_SESSION['propositions_meme_residence'];
				if(count($data['propositions_meme_type']) > 0 || count($data['propositions_meme_residence']) > 0)
				{
					$this->render("propositions", $data);
				}
				else
				{
					$this->redirect("page=demand");
				}
			}
			else
			{
				$this->redirect("page=connection");
			}
		}

		//Refuser une proposition d'appartement de meme type par l'utilisateur
		public function refuse_proposition_type(){
			if($this->isConnected())
			{
				if($this->exists("idappartement"))
				{
					foreach($_SESSION['propositions_meme_type'] as $key=>$value)
					{
						if($value["idappartement"] == $_POST["idappartement"])
						{
							unset($_SESSION['propositions_meme_type'][$key]);
							break;
						}
					}
					if((empty($_SESSION['propositions_meme_type'])) && (empty($_SESSION['propositions_meme_residence'])))
					{
						$this->redirect("page=demand");
					}
					else
					{
						$this->index();
					}
				}
				else
				{
					$data["error_message"] = "erreur, les champs n'ont pas tous été renseignés";
				}
			}
			else
			{
				$data["error_message"] = "Vous devez être connecté pour accéder à cette page";
			}
		}

		//Refuser une proposition d'appartement de meme residence par l'utilisateur
		public function refuse_proposition_residence(){
			if($this->isConnected())
			{
				if($this->exists("idappartement"))
				{
					foreach($_SESSION['propositions_meme_residence'] as $key=>$value)
					{
						if($value["idappartement"] == $_POST["idappartement"])
						{
							unset($_SESSION['propositions_meme_residence'][$key]);
							break;
						}
					}
					if(empty($_SESSION['propositions_meme_residence']))
					{
						$this->redirect("page=demand");
					}
					else
					{
						$this->index();
					}
				}
				else
				{
					$data["error_message"] = "erreur, les champs n'ont pas tous été renseignés";
					$this->redirect("page=demand");
				}
			}
			else
			{
				$data["error_message"] = "Vous devez être connecté pour accéder à cette page";
				$this->redirect("page=connection");
			}
		}

		//Accepter une proposition par l'utilisateur
		public function accept(){
			if($this->isConnected())
			{
				if($this->exists("idappartement") && $this->exists("idpersonne"))
				{
					$this->model->add_locataire($_POST['idpersonne'], $_POST['idappartement']);
					$this->redirect("page=home");
				}
				else
				{
					$data["error_message"] = "erreur, les champs n'ont pas tous été renseignés";
					$this->redirect("page=demand");
				}
			}
			else
			{
				$data["error_message"] = "Vous devez être connecté pour accéder à cette page";
				$this->redirect("page=connection");
			}
		}
	}

?>