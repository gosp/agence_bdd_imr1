<?php
	
	class DemandController extends BaseController{

		public function __construct()
		{
			parent::__construct();
		}

		public function index()
		{
			if($this->isConnected())
			{
				$this->load_demand_with_data();
			}
			else
			{
				$this->redirect("page=connection");
			}
		}

		/**
		 * Ajoute une demande et la traite
		 */
		public function newDemand()
		{
			if($this->isConnected())
			{
				if($this->exists("adresse") && $this->exists("telephone"))
				{
					//Ajoute la demande dans la bdd
					$this->model->add_demande($_POST['type'], $_POST['residence'], $_POST['adresse'], $_POST['telephone'], date("Y-m-d"));	
					
					$matchingAppartement = $this->model->recherche_appartement_residence_type_libre($_POST['residence'], $_POST['type']);
					//S'il existe un appartement libre correspondant à la demande
					if(count($matchingAppartement) != 0){
						$this->model->add_locataire($_SESSION['idpersonne'], $matchingAppartement);
					}
					else
					{
						//Affichage des propositions à l'utilisateur
						$_SESSION['propositions_meme_type'] = $this->model->recherche_appartements_type_libres($_POST['type']);
						$_SESSION['propositions_meme_residence'] = $this->model->recherche_appartements_residence_libres($_POST['residence']);
						$this->redirect("page=propositions");
					}
				}
				else
				{
					//Afficher erreur tous les champs n'ont pas été renseignés ==> NE MARCHE PAS
					$data["error_message"] = "erreur, les champs n'ont pas tous été renseignés";
					$this->load_demand_with_data();
				}
				$this->load_demand_with_data();
			}
			else
			{
				$this->redirect("page=connexion");
			}
		}

		/**
		 * Affichage de la page demande avec chargement des données nécessaires
		 */
		function load_demand_with_data(){
			$data['residences'] = $this->model->get_all_residence();
			$data['types'] = $this->model->get_all_type();
			$data['informations_demandes'] = $this->model->get_user_demands($_SESSION['idpersonne']);
			$data['nombre_demandes'] = $this->model->get_user_demands_informations($_SESSION['idpersonne']);
			$this->render("demande", $data);
		}

		/**
		 * Supprime une demande
		 */
		function delete()
		{
			if($this->isConnected())
			{
				if($this->exists("idresidence") && $this->exists("idtype"))
				{
					$this->model->delete_demand($_SESSION["idpersonne"], $_POST["idresidence"], $_POST["idtype"]);
					$this->load_demand_with_data();
				}
			}
			else
			{
				$this->redirect("page=connexion");
			}
		}

	}

?>