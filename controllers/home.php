<?php
	
	class HomeController extends BaseController{

		public function __construct()
		{
			parent::__construct();
		}

		public function index()
		{
			if($this->isConnected())
			{
				$data['location'] = $this->model->get_user_location_informations($_SESSION['idpersonne']);

				$data['demandes'] = $this->model->get_user_demands_informations($_SESSION['idpersonne']);

				$data['nom'] = $_SESSION['nom'];
				$data['prenom'] = $_SESSION['prenom'];
				$data['identifiant'] = $_SESSION['idpersonne'];

				$this->render("accueil", $data);
			}
			else
			{
				$this->redirect("page=connection");
			}
		}
	}

?>