<?php
	
	class BaseController{

		private $view_path = "views/";
		private $base_path = "http://hlm.local/";
		protected $model = null;

		public function __construct()
		{
			$this->model = new MainModel();
		}

		/**
		 * Récupère la connexion à la base de données
		 * @return Connection
		 */
		public function getConnection()
		{
			return Connection::getInstance();
		}

		/**
		 * Affiche la page (vue)
		 * @param String $page_template le nom du fichier vue
		 * @param Array les données à fournir à la vue
		 */
		public function render($page_template, $data = array())
		{
			include_once($this->view_path.$page_template.".php");
		}

		/**
		 * Test si une variable POST existe
		 * @param String $var
		 * @return Boolean
		 */
		public function exists($var)
		{
			if(isset($_POST[$var]) && $_POST[$var] != "")
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/**
		 * Test si l'utilisation est connecté
		 * Voir session
		 * @return Boolean
		 */
		public function isConnected()
		{
			if(isset($_SESSION['connected']) && $_SESSION['connected'] == true)
			{
				return true;
			}
			else return false;
		}

		/**
		 * Redirige l'utilisateur vers la page demandée
		 * @param String $link les liens exemple page=demand&action=xx
		 */
		public function redirect($link)
		{
			header("Location: ".$this->base_path."index.php?".$link);
		}
	}

?>