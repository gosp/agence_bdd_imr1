<?php
	
	class LiberationController extends BaseController{

		public function __construct()
		{
			parent::__construct();
		}

		public function index()
		{
			if($this->isConnected())
			{
				if(isset($_POST["idlocataire"]) && !empty($_POST["idlocataire"]))
				{
					$data['infos_personne'] = $this->model->get_locataire_informations($_POST["idlocataire"]);
				}
				else
				{
					$data['infos_personne'] = null;
				}
				$this->render("liberation", $data);
			}
			else
			{
				$this->redirect("page=connection");
			}
		}

		/**
		 * Libérer un appartement (le locataire en $_POST)
		 */
		public function liberate()
		{
			if($this->isConnected())
			{
				if(isset($_POST["idSupprime"]))
				{
					$this->model->delete_locataire($_POST["idSupprime"]);
					$this->render("liberation");
				}
				else
				{
					$data["error_message"] = "erreur, les champs n'ont pas tous été renseignés";
					$this->redirect("page=liberate");
				}
			}
			else
			{
				$this->redirect("page=connection");
			}
		}
	}

?>