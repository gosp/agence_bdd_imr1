<?php
	
	class ConnectionController extends BaseController{

		public function __construct()
		{
			parent::__construct();
		}

		public function index()
		{
			$this->render("connexion");
		}

		/**
		 * Connexion d'un utilisateur à l'application web
		 */
		public function connect()
		{
			if($this->exists("idpersonne") && $this->exists("nom"))
			{
				$result = $this->model->connect($_POST["idpersonne"], $_POST["nom"]);

				if($result != false)
				{
					$_SESSION['idpersonne'] = $_POST['idpersonne'];
					$_SESSION['connected'] = true;
					$_SESSION['nom'] = $result['nom'];
					$_SESSION['prenom'] = $result['prenom'];
					$this->redirect("page=home");
				}
				else
				{
					$data["error_message"] = "erreur, veuillez recommencer";
					$this->render("connexion", $data);
				}
			}
			else
			{
				$data["error_message"] = "erreur, veuillez recommencer";
				$this->render("connexion", $data);
			}
		}

		/**
		 * Inscription d'un utilisateur à l'application web
		 */
		public function sign()
		{
			if($this->exists("prenom") && $this->exists("nom"))
			{
				$result = $this->model->sign($_POST["prenom"], $_POST["nom"]);

				if($result != false)
				{
					$data["success_message"] = "Inscription réussie, veuillez vous connecter";
					$this->render("connexion", $data);
				}
				else
				{
					$data["error_message"] = "erreur, veuillez recommencer";
					$this->render("connexion", $data);
				}
			}
			else
			{
				$data["error_message"] = "erreur, veuillez recommencer";
				$this->render("connexion", $data);
			}
		}

		/**
		 * Déconnexion
		 */
		public function disconnect(){
			session_destroy();
			$this->render("connexion");
		}
	}

?>