<?php
	
	class waitingListController extends BaseController{

		public function __construct()
		{
			parent::__construct();
		}

		public function index($type = "T1")
		{
			if($this->isConnected())
			{
				$data['types'] = $this->model->get_all_type();
				$data['demandes'] = $this->model->get_all_users_demands($type);

				$this->render("listeAttente", $data);
			}
			else
			{
				$this->redirect("page=connection");
			}
		}

		/**
		 * Recherche la liste d'attente pour un type d'appartement donné
		 */
		public function search()
		{
			if($this->exists('type') && $_POST['type'] != 'tous')
			{
				$this->index($_POST['type']);
			}
			else
			{
				$this->index();
			}
		}
	}

?>