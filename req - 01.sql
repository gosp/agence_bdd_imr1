SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema hlm
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `hlm` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `hlm` ;

-- -----------------------------------------------------
-- Table `hlm`.`personne`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hlm`.`personne` ;

CREATE TABLE IF NOT EXISTS `hlm`.`personne` (
  `idpersonne` INT NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NULL,
  `prenom` VARCHAR(45) NULL,
  PRIMARY KEY (`idpersonne`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hlm`.`type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hlm`.`type` ;

CREATE TABLE IF NOT EXISTS `hlm`.`type` (
  `idtype` INT NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NOT NULL,
  `loyer` FLOAT NOT NULL,
  PRIMARY KEY (`idtype`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hlm`.`residence`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hlm`.`residence` ;

CREATE TABLE IF NOT EXISTS `hlm`.`residence` (
  `idresidence` INT NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idresidence`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hlm`.`appartement`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hlm`.`appartement` ;

CREATE TABLE IF NOT EXISTS `hlm`.`appartement` (
  `idappartement` INT NOT NULL AUTO_INCREMENT,
  `idtype` INT NOT NULL,
  `idresidence` INT NOT NULL,
  PRIMARY KEY (`idappartement`),
  INDEX `fk_appartement_type1_idx` (`idtype` ASC),
  INDEX `fk_appartement_residence1_idx` (`idresidence` ASC),
  CONSTRAINT `fk_appartement_type1`
    FOREIGN KEY (`idtype`)
    REFERENCES `hlm`.`type` (`idtype`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_appartement_residence1`
    FOREIGN KEY (`idresidence`)
    REFERENCES `hlm`.`residence` (`idresidence`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hlm`.`locataire`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hlm`.`locataire` ;

CREATE TABLE IF NOT EXISTS `hlm`.`locataire` (
  `idlocataire` INT NOT NULL AUTO_INCREMENT,
  `date_entree` DATE NOT NULL,
  `idappartement` INT NOT NULL,
  PRIMARY KEY (`idlocataire`),
  INDEX `fk_locataire_appartement1_idx` (`idappartement` ASC),
  UNIQUE INDEX `idappartement_UNIQUE` (`idappartement` ASC),
  CONSTRAINT `fk_locataire_personne1`
    FOREIGN KEY (`idlocataire`)
    REFERENCES `hlm`.`personne` (`idpersonne`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_locataire_appartement1`
    FOREIGN KEY (`idappartement`)
    REFERENCES `hlm`.`appartement` (`idappartement`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hlm`.`demande`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hlm`.`demande` ;

CREATE TABLE IF NOT EXISTS `hlm`.`demande` (
  `idpersonne` INT NOT NULL,
  `idtype` INT NOT NULL,
  `idresidence` INT NOT NULL,
  `adresse` VARCHAR(100) NOT NULL,
  `telephone` CHAR(10) NOT NULL,
  `date` DATE NOT NULL,
  INDEX `fk_demande_personne_idx` (`idpersonne` ASC),
  INDEX `fk_demande_type1_idx` (`idtype` ASC),
  INDEX `fk_demande_residence1_idx` (`idresidence` ASC),
  PRIMARY KEY (`idpersonne`, `idtype`, `idresidence`),
  CONSTRAINT `fk_demande_personne`
    FOREIGN KEY (`idpersonne`)
    REFERENCES `hlm`.`personne` (`idpersonne`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_demande_type1`
    FOREIGN KEY (`idtype`)
    REFERENCES `hlm`.`type` (`idtype`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_demande_residence1`
    FOREIGN KEY (`idresidence`)
    REFERENCES `hlm`.`residence` (`idresidence`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

USE `hlm` ;

-- -----------------------------------------------------
-- Placeholder table for view `hlm`.`appartements_libres`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hlm`.`appartements_libres` (`idappartement` INT, `idtype` INT, `idresidence` INT, `nom_type` INT, `nom_residence` INT);

-- -----------------------------------------------------
-- function nb_demande_personne
-- -----------------------------------------------------

USE `hlm`;
DROP function IF EXISTS `hlm`.`nb_demande_personne`;

DELIMITER $$
USE `hlm`$$
CREATE FUNCTION `nb_demande_personne` (ID INT(11))
RETURNS int
DETERMINISTIC
BEGIN
	DECLARE nb INT;
	SELECT COUNT(*) INTO nb
	FROM demande
	WHERE idpersonne = ID;
	RETURN nb;


END;
$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure nouveau_locataire
-- -----------------------------------------------------

USE `hlm`;
DROP procedure IF EXISTS `hlm`.`nouveau_locataire`;

DELIMITER $$
USE `hlm`$$
CREATE PROCEDURE `nouveau_locataire` (ID_LOCATAIRE INT(11), ID_APPARTEMENT INT(11))
BEGIN
	-- Autorise la recursivité
	SET @@GLOBAL.max_sp_recursion_depth=255;
	SET @@session.max_sp_recursion_depth=255;
	-- On le supprime
	CALL liberer_appartement(ID_LOCATAIRE);

	-- On l'insere de nouveau dans locataire avec nouvel appartement
	INSERT INTO locataire VALUES (ID_LOCATAIRE, NOW(), ID_APPARTEMENT);
	-- On supprime ses demandes
	DELETE FROM demande WHERE idpersonne=ID_LOCATAIRE;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure liberer_appartement
-- -----------------------------------------------------

USE `hlm`;
DROP procedure IF EXISTS `hlm`.`liberer_appartement`;

DELIMITER $$
USE `hlm`$$
CREATE PROCEDURE `liberer_appartement` (ID_LOCATAIRE INT (11))
BEGIN
	DECLARE id_appartement INT;
	DECLARE id_type INT;
	DECLARE id_residence INT;
	DECLARE id_nouveau_locataire INT;
	-- Autorise la recursivité
	SET @@GLOBAL.max_sp_recursion_depth=255;
	SET @@session.max_sp_recursion_depth=255;
	
	-- récupération de l'id de l'appartement libéré
	SELECT idappartement FROM locataire WHERE idlocataire=ID_LOCATAIRE INTO id_appartement;
	-- Suppression du locataire
	DELETE FROM locataire WHERE idlocataire = ID_LOCATAIRE;
	-- Récupération de l'id du type d'appartement libéré
	SELECT idtype FROM appartement WHERE idappartement=id_appartement INTO id_type;
	-- Récupération de l'id de la résidence de l'appartement libéré
	SELECT idresidence FROM appartement WHERE idappartement=id_appartement INTO id_residence;
	-- Récupération de l'id de la personne ayant une demande correspondant à l'appartement libéré (la plus prioritaire) stockée dans id_nouveau_locataire
	SELECT idpersonne FROM demande WHERE idtype=id_type AND idresidence=id_residence ORDER BY demande.date LIMIT 1 INTO id_nouveau_locataire;
	-- Si il y a un nouveau locataire
	IF((SELECT COUNT(*) FROM ((SELECT idpersonne FROM demande WHERE idtype=id_type AND idresidence=id_residence ORDER BY demande.date LIMIT 1) as idNouveauLocataire)) <> 0) THEN 
		-- appel à la procédure nouveau_locataire
		CALL nouveau_locataire(id_nouveau_locataire, id_appartement);
	END IF;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- function recherche_appartement_residence_type
-- -----------------------------------------------------

USE `hlm`;
DROP function IF EXISTS `hlm`.`recherche_appartement_residence_type`;

DELIMITER $$
USE `hlm`$$
CREATE FUNCTION `recherche_appartement_residence_type` (ID_RESIDENCE INT(11), ID_TYPE INT(11))
RETURNS INT
BEGIN
	DECLARE res INT;
	SELECT appartement.idappartement FROM appartement WHERE idappartement NOT IN (SELECT locataire.idappartement FROM locataire) AND appartement.idresidence = ID_RESIDENCE AND appartement.idtype = ID_TYPE LIMIT 1 into res;
	RETURN res;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- function statistiques_globales
-- -----------------------------------------------------

USE `hlm`;
DROP function IF EXISTS `hlm`.`statistiques_globales`;

DELIMITER $$
USE `hlm`$$
CREATE FUNCTION `statistiques_globales`()
RETURNS INT
BEGIN
	DECLARE nb_appart_occupes INT;
	DECLARE nb_appart_libres INT;

	SELECT count(*) FROM locataire INTO nb_appart_occupes;
	SELECT count(*) FROM appartement INTO nb_appart_libres;
	return round((nb_appart_occupes/nb_appart_libres)*100);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- function statistiques_residence
-- -----------------------------------------------------

USE `hlm`;
DROP function IF EXISTS `hlm`.`statistiques_residence`;

DELIMITER $$
USE `hlm`$$
CREATE FUNCTION `statistiques_residence` (ID_RESIDENCE INT(11))
RETURNS INT
BEGIN
	DECLARE nb_appart_occupes INT;
	DECLARE nb_appart_libres INT;

	SELECT count(*) FROM appartement WHERE idresidence=ID_RESIDENCE INTO nb_appart_libres;
	SELECT count(*) FROM locataire WHERE idappartement IN (SELECT idappartement FROM appartement WHERE idresidence=ID_RESIDENCE) INTO nb_appart_occupes;
	return round((nb_appart_occupes/nb_appart_libres)*100);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- View `hlm`.`appartements_libres`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `hlm`.`appartements_libres` ;
DROP TABLE IF EXISTS `hlm`.`appartements_libres`;
USE `hlm`;
CREATE  OR REPLACE VIEW `appartements_libres` 
AS SELECT appartement.idappartement, appartement.idtype, appartement.idresidence, type.nom as nom_type, residence.nom as nom_residence
 FROM (appartement 
INNER JOIN hlm.type ON appartement.idtype=type.idtype) 
INNER JOIN residence ON appartement.idresidence=residence.idresidence
	WHERE appartement.idappartement NOT IN(SELECT idappartement FROM locataire);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
USE `hlm`;

DELIMITER $$

USE `hlm`$$
DROP TRIGGER IF EXISTS `hlm`.`demande_BINS` $$
USE `hlm`$$
CREATE TRIGGER `demande_BINS` BEFORE INSERT ON `demande` FOR EACH ROW
begin
	if (SELECT COUNT(*) FROM demande WHERE idpersonne = new.idpersonne) >= 2 then
		signal sqlstate '45000'
		set message_text = "Erreur trop de demandes (max : 2)",
		mysql_errno = 10;
	end if;
end;
$$


DELIMITER ;
