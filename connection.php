<?php

class Connection {

    private static $instance = null;
    private $db = null;

    public static function getInstance(){

        if(self::$instance == null)
        {
            self::$instance = new Connection();
        }

        return self::$instance;
    }

    private function __construct()
    {
        $bdd = array (
            "host"   => "localhost",
            "user"   => "root",
            "passwd" => "",
            "base"   => "hlm"
        );

        try
        {
            $db = new PDO("mysql:host={$bdd['host']};dbname={$bdd['base']}", $bdd['user'], $bdd['passwd']);
            $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $this->db = $db;
        }
        catch(PDOException $e)
        {
            echo "Erreur : {$e->getMessage()}\n";
            echo "N° : {$e->getCode()}\n";
            exit;
        }
    }

    public function getDB()
    {
        return $this->db;
    }

}

?>