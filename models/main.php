﻿<?php
	
	class MainModel{

		private $db;

		public function __construct()
		{
			$this->db = Connection::getInstance()->getDB();
		}

		/**
		 * Récupère la connexion à la base de données
		 * @param  Integer $idpersonne
		 * @param  String $nom 
		 * @return Array(idPersonne, nom, prenom) | array() 
		 */
		public function connect($idpersonne, $nom)
		{
			$req = "SELECT * FROM personne 
			WHERE idPersonne = {$this->db->quote($idpersonne)}
			AND nom = {$this->db->quote($nom)}";

	        try
	        {
	            $res = $this->db->query($req);

	            $output = $res->fetch();

	            if(count($output) <= 0)
	      		{
	      			return array();
	      		}
	      		else return $output;
	        }
	        catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }
		}

		/**
		 * Récupère toutes les résidences
		 * @return Array(idResidence, nom) | array() 
		 */
		public function get_all_residence()
		{
			$req = "SELECT idresidence, nom FROM residence";

	        try
	        {
	            $res = $this->db->query($req);

	            $output = $res->fetchAll();

	            if(count($output) <= 0)
	      		{
	      			return array();
	      		}
	      		else return $output;
	        }
	        catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }
		}

		/**
		 * Récupère les information de l'utilisateur 
		 * sur l'appartement qu'il possède s'il en possède un
		 * @return Array(idResidence, nom) | array() 
		 */
		public function get_user_location_informations($user_id)
		{
			$req = "SELECT l.idappartement AS id_appartement, l.date_entree AS date_entree, t.nom AS nom_type ,t.loyer AS loyer, r.nom AS nom_residence 
			FROM locataire as l
			INNER JOIN appartement AS a ON a.idappartement = l.idappartement
			INNER JOIN residence AS r ON a.idresidence = r.idresidence
			INNER JOIN type AS t ON a.idtype = t.idtype 
			WHERE idlocataire = {$this->db->quote($user_id)}
			LIMIT 1";

	        try
	        {
	            $res = $this->db->query($req);

	            $output = $res->fetch();

	            if(count($output) <= 0)
	      		{
	      			return false;
	      		}
	      		else return $output;
	        }
	        catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            echo "lol";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }
		}

		/**
		 * Récupère la liste des demandes d'un utilisateur
		 * pour un utilisateur donné
		 * @param  Integer $user_id l'id de l'utilisateur
		 * @return Array
		 */
		public function get_user_demands($user_id)
		{
			$req = "SELECT t.idtype AS id_type, r.idresidence AS id_residence, t.nom AS nom_type, r.nom AS nom_residence, d.adresse as adresse, d.telephone as telephone, d.date as date_demande FROM demande as d
			INNER JOIN residence AS r ON d.idresidence = r.idresidence
			INNER JOIN type AS t ON d.idtype = t.idtype 
			WHERE idpersonne = {$this->db->quote($user_id)}";

	        try
	        {
	            $res = $this->db->query($req);

	            $output = $res->fetchAll();

	            if(count($output) <= 0)
	      		{
	      			return array();
	      		}
	      		else return $output;
	        }
	        catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }			
		}

		/**
		 * Récupère la liste de toutes les demandes
		 * des utilisateurs ordonné par date de demande
		 * pour un type donné
		 * @return Array
		 */
		public function get_all_users_demands($type)
		{
			$req = "SELECT t.nom AS nom_type, r.nom AS nom_residence, d.date as date_demande, p.nom AS nom, p.prenom AS prenom FROM demande as d
			INNER JOIN residence AS r ON d.idresidence = r.idresidence
			INNER JOIN personne AS p ON d.idpersonne = p.idpersonne
			INNER JOIN type AS t ON d.idtype = t.idtype 
			WHERE t.nom = {$this->db->quote($type)}
			ORDER BY d.date";

	        try
	        {
	            $res = $this->db->query($req);

	            $output = $res->fetchAll();

	            if(count($output) <= 0)
	      		{
	      			return array();
	      		}
	      		else return $output;
	        }
	        catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }			
		}

		/**
		 * Récupère des information sur le nombre de demandes
		 * faites par un utilisateur
		 * @param  Integer $user_id 
		 * @return Integer nombre de demandes
		 */
		public function get_user_demands_informations($user_id) 
		{
			$req = "SELECT count(*) AS nb FROM demande
			WHERE idpersonne = {$this->db->quote($user_id)}
			LIMIT 1";

	        try
	        {
	            $res = $this->db->query($req);

	            $output = $res->fetch();

				return $output['nb'];
	        }
	        catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }				
		}

		/**
		 * Récupère tous les types
		 * @return Array(idtype, nom, loyer) | array() 
		 */
		public function get_all_type()
		{
			$req = "SELECT idtype, nom, loyer FROM type";

	        try
	        {
	            $res = $this->db->query($req);

	            $output = $res->fetchAll();

	            if(count($output) <= 0)
	      		{
	      			return array();
	      		}
	      		else return $output;
	        }
	        catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }
		}

		/**
		 * Inscrit un utilisateur
		 * @param  String $prenom    
		 * @param  String $nom 
		 * @return Boolean true | false
		 */
		public function sign($nom, $prenom)
		{
			$req = "INSERT INTO personne (idpersonne, nom, prenom)
			VALUES ('', {$this->db->quote($nom)}, {$this->db->quote($prenom)})";

	        try
	        {
	        	
	            $this->db->query($req);
	            return true;
	        }
	        catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }
		}		

		/**
		 * Ajoute une demande
		 * @param  String $idType       
		 * @param  String $idResidence   
		 * @param  String $adresse 
		 * @param  String $tel
		 * @param  String $date
		 */
		public function add_demande($idType, $idResidence, $adresse, $tel, $date)
		{
			$req = "INSERT INTO demande VALUES(
				{$this->db->quote($_SESSION['idpersonne'])}, 
				{$this->db->quote($idType)},
				{$this->db->quote($idResidence)},
				{$this->db->quote($adresse)},
				{$this->db->quote($tel)},
				{$this->db->quote($date)})";

	        try
	        {
	            $res = $this->db->query($req);
	        }
	        catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }
		}


		/**
		 * Recherche les appartements libres d'une résidence et d'un type donnés
		 * @param  String $idresidence
		 * @param  String $idtype
		 */
		
		public function recherche_appartement_residence_type_libre($idresidence, $idtype)
		{
			$req = "SELECT recherche_appartement_residence_type($idresidence, $idtype) as idappartement";
	        try
	        {
	            $res = $this->db->query($req);
	            $output = $res->fetchAll();

	            if(count($output) <= 0)
	      		{
	      			return array();
	      		}
	      		else return $output[0]['idappartement'];
	        }
	        catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }
		}

		/**
		 * Inscrit un locataire
		 * @param  String $idlocataire    
		 * @param  String $idappartement
		 */
		public function add_locataire($idlocataire, $idappartement)
		{
			$req = "CALL nouveau_locataire($idlocataire, $idappartement)";

	        try
	        {
	            $res = $this->db->query($req);
	        }
	        catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }
		}

		/**
		 * Vérifie si une personne est locataire
		 * @param String $idpersonne
		 */
		public function locataire_exists($idpersonne)
		{
			$req = "SELECT idlocataire FROM locataire where idlocataire=$idpersonne";

	        try
	        {
	            $res = $this->db->query($req);
	            $output = $res->fetchAll();
	            if(count($output) <= 0)
	      		{
	      			return false;
	      		}
	      		else return true;
	        }
	        catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }
		}

		/**
		 * Supprime un locataire
		 * @param String $idpersonne
		 */
		public function delete_locataire($idpersonne)
		{
			$req = "CALL liberer_appartement($idpersonne)";

	        try
	        {
	            $res = $this->db->query($req);
	            return true;
	        }
	        catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }
		}

		/**
		 * Vérifie si des demandes existent pour une personne
		 * @param String $idpersonne
		 */
		public function demand_exists_person($idpersonne)
		{
			$req = "SELECT * FROM demande where idpersonne=$idpersonne";

	        try
	        {
	            $res = $this->db->query($req);
	            $output = $res->fetchAll();
	            if(count($output) <= 0)
	      		{
	      			return false;
	      		}
	      		else return true;
	        }
	        catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }
		}

		/**
		 * Supprime les demandes d'une personne
		 * @param String $idpersonne
		 */
		public function delete_demand_person($idpersonne)
		{
			$req = "DELETE FROM demande where idpersonne=$idpersonne";

	        try
	        {
	            $res = $this->db->query($req);
	            return true;
	        }
	        catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }
		}

		/**
		 * Supprime une demande
		 * @param String $idpersonne
		 * @param String $idresidence
		 * @param String $idtype
		 */
		public function delete_demand($idpersonne, $idresidence, $idtype)
		{
			$req = "DELETE FROM demande WHERE idpersonne=$idpersonne AND idresidence=$idresidence AND idtype=$idtype";

	        try
	        {
	            $res = $this->db->query($req);
	            return true;
	        }
	        catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }
		}

		/**
		 * Recherche les appartements libres d'un type donné
		 * @param  String $idtype
		 */
		public function recherche_appartements_type_libres($idtype)
		{
			$req = "SELECT * FROM appartements_libres WHERE idtype=$idtype";
	        try
	        {
	            $res = $this->db->query($req);
	            $output = $res->fetchAll();
	            if(count($output) <= 0)
	      		{
	      			return array();
	      		}
	      		else return $output;
	        }
	        catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }
		}

		/**
		 * Recherche les appartements libres d'une résidence donnée
		 * @param  String $idresidence
		 */
		public function recherche_appartements_residence_libres($idresidence)
		{
			$req = "SELECT * FROM appartements_libres WHERE idresidence=$idresidence";
	        try
	        {
	            $res = $this->db->query($req);
	            $output = $res->fetchAll();
	            if(count($output) <= 0)
	      		{
	      			return array();
	      		}
	      		else return $output;
	        }
	        catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }
		}


		/**
		 * Récupère les statistiques d'occupation globale des appartements
		 * @return pourcentage de statistiques
		 */
		public function get_global_occupation_statistics() 
		{
			$req = "SELECT statistiques_globales() as stat_globales";
	        try
	        {
	            $res = $this->db->query($req);
	            $output = $res->fetch();
				return $output['stat_globales'];
	        }
	        catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }				
		}

		/**
		 * Récupère les statistiques d'occupation de chaque residence
		 * @return pourcentage de statistiques
		 */
		public function get_each_residence_occupation_statistics() 
		{
			try
	        {
	       		$result_array = array();
				$req1 = "SELECT idresidence, nom from residence";
				$res1 = $this->db->query($req1);
		        $output1 = $res1->fetchAll();

		        //Pour chaque residence
		        foreach($output1 as $key)
		        {
		        	//Récupère le taux d'occupation de la résidence
		        	$req2 = "SELECT statistiques_residence($key[idresidence]) as stat_residence";
		        	$res2 = $this->db->query($req2);
		        	$output2 = $res2->fetch();
		        	array_push($result_array, array("residence" => $key['nom'], "percentage" => $output2['stat_residence']));
		        }
		        return $result_array;
		    }
		    catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }				
		}


		/**
		 * Récupère les noms et prénoms d'une personne
		 * @param  String : $idpersonne
		 * @return Array(idpersonne, nom, prenom) | array() 
		 */
		public function get_locataire_informations($idpersonne)
		{
			$req = "SELECT idpersonne, nom, prenom FROM personne WHERE idpersonne=$idpersonne AND idpersonne IN (SELECT idlocataire FROM locataire)";

	        try
	        {
	            $res = $this->db->query($req);

	            $output = $res->fetchAll();

	            if(count($output) <= 0)
	      		{
	      			return array();
	      		}
	      		else return $output[0];
	        }
	        catch(PDOException $e)
	        {
	            echo "KO\n{$e->getMessage()}\n";
	            echo "N° : {$e->getCode()}\n";
	            echo "<pre>";
	            print_r($this->db->errorInfo());
	            echo "</pre>";
	        }
		}

	}

?>