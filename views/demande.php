﻿<html>
    <header>
       <title>Demandes</title>
       <link rel="stylesheet" type="text/css" href="style/main.css"/>
    </header>
    <body> 
        <div id="content">
            <?php require_once("menu.php"); ?>
            <h4>Liste de vos demandes en attente</h4>
            <!-- Si aucune demande -->
            <?php if($data['nombre_demandes'] == 0):?>
                Vous n'avez aucune demande en attente.
            <!-- Si au moins une demande -->
            <?php elseif($data['nombre_demandes'] == 1):?>
                Vous avez 1 demande en attente.
            <?php else:
                echo "Vous avez ".$data['nombre_demandes']." demandes en attente.";
            ?>
            <?php endif;?>
            <?php if($data['nombre_demandes'] > 0): ?>
            <br>
                <table>
                    <thead>
                        <th>Type</th>
                        <th>Résidence</th>
                        <th>Adresse</th>
                        <th>Téléphone</th>
                        <th>Date</th>
                    </thead>
                    <tbody>
                    <?php foreach($data['informations_demandes'] as $key):?>
                    <tr>
                        <td>
                            <?php echo $key['nom_type']; ?>
                        </td>
                        <td>
                            <?php echo $key['nom_residence']; ?>
                        </td>
                        <td>
                            <?php echo $key['adresse']; ?>
                        </td>
                        <td>
                            <?php echo $key['telephone']; ?>
                        </td>
                        <td>
                            <?php echo $key['date_demande']; ?>
                        </td>
                        <td>
                        <form method="POST" action="index.php?page=demand&action=delete">
                            <input type="hidden" name="idtype" value="<?php echo $key['id_type'];?>"/>
                            <input type="hidden" name="idresidence" value="<?php echo $key['id_residence'];?>"/>
                            <input type="submit" value="Supprimer la demande"/>
                        </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
                </table>
            <?php endif ?>
            <br/><br/>
            <hr>
            <h4>Effectuer une nouvelle demande</h4>
            <!-- Si il y a moins de 2 demandes -->
            <?php if($data['nombre_demandes'] < 2):?>
               <!-- formulaire de demandes -->
               <form method="POST" action="index.php?page=demand&action=newDemand">
                <table>
                    <tr>
                        <td>
                            <label>Veuillez spécifier une demande :</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="residence">Résidence :</label>
                        </td>
                        <td>
                            <SELECT name="residence">
                                <?php foreach ($data['residences'] as $residence): ?>
                                    <OPTION VALUE="<?php echo $residence['idresidence']; ?>"><?php echo $residence['nom']; ?></OPTION>
                                <?php endforeach ?>
                            </SELECT>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="type">Type :</label>
                        </td>
                        <td>
                            <SELECT name="type">
                                <?php foreach ($data['types'] as $type): ?>
                                    <OPTION VALUE="<?php echo $type['idtype']; ?>"><?php echo $type['nom']; ?></OPTION>
                                <?php endforeach ?>
                            </SELECT>
                         </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="adresse">Adresse : </label>
                        </td>
                        <td>
                            <input type="text" name="adresse">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="adresse">Téléphone : </label>
                        </td>
                        <td>
                            <input type="text" name="telephone">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="submit" value="Valider">
                        </td>
                    </tr>
                </table>
            <?php else:?>
                Vous avez déjà formulé 2 demandes, vous ne pouvez pas en formuler de troisième.
            <?php endif; ?>
           </form>
        </div>
    </body>
</html>