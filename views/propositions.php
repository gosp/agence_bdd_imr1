﻿<html>
    <header>
       <title>Propositions</title>
       <link rel="stylesheet" type="text/css" href="style/main.css"/>
    </header>
    <body> 
        <div id="content">
            <?php require_once("menu.php"); ?>
            <h4>Aucun appartement du type et de la résidence désirés n'est disponible..</h4>
            <?php if(count($data['propositions_meme_type']) > 0): ?>
            <h5>Voici les propositions d'appartements de même type</h5>
            <br>
                <table>
                    <thead>
                        <th>N° </th>
                        <th>Type</th>
                        <th>Résidence</th>
                    </thead>
                    <tbody>
                    <?php foreach($data['propositions_meme_type'] as $key):?>
                    <tr>
                        <td>
                            <?php echo $key['idappartement']; ?>
                        </td>
                        <td>
                            <?php echo $key['nom_type']; ?>
                        </td>
                        <td>
                            <?php echo $key['nom_residence']; ?>
                        </td>
                        <td>
                        <form method="POST" action="index.php?page=propositions&action=refuse_proposition_type">
                            <input type="hidden" name="idappartement" value="<?php echo $key['idappartement'];?>"/>
                            <input type="submit" value="Refuser"/>
                        </form>
                        </td>
                        <td>
                        <form method="POST" action="index.php?page=propositions&action=accept">
                            <input type="hidden" name="idpersonne" value="<?php echo $_SESSION['idpersonne'];?>"/>
                            <input type="hidden" name="idappartement" value="<?php echo $key['idappartement'];?>"/>
                            <input type="submit" value="Accepter"/>
                        </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
                </table>
            <?php elseif((count($data['propositions_meme_type']) == 0) && (count($data['propositions_meme_residence']) > 0)): ?>
                <h5>Voici les propositions d'appartements de même residence</h5>
                <br>
                <table>
                    <thead>
                        <th>N° </th>
                        <th>Type</th>
                        <th>Résidence</th>
                    </thead>
                    <tbody>
                    <?php foreach($data['propositions_meme_residence'] as $key):?>
                    <tr>
                        <td>
                            <?php echo $key['idappartement']; ?>
                        </td>
                        <td>
                            <?php echo $key['nom_type']; ?>
                        </td>
                        <td>
                            <?php echo $key['nom_residence']; ?>
                        </td>
                        <td>
                        <form method="POST" action="index.php?page=propositions&action=refuse_proposition_residence">
                            <input type="hidden" name="idappartement" value="<?php echo $key['idappartement'];?>"/>
                            <input type="submit" value="Refuser"/>
                        </form>
                        </td>
                        <td>
                        <form method="POST" action="index.php?page=propositions&action=accept">
                            <input type="hidden" name="idpersonne" value="<?php echo $_SESSION['idpersonne'];?>"/>
                            <input type="hidden" name="idappartement" value="<?php echo $key['idappartement'];?>"/>
                            <input type="submit" value="Accepter"/>
                        </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
                </table>
            <?php endif ?>
            <br/><br/>
        </div>
    </body>
</html>