﻿<html>
    <header>
       <title>Statistiques</title>     
       <link rel="stylesheet" type="text/css" href="style/main.css" />
    </header>
    <body>
        <div id="content">            
        <?php require_once("menu.php"); ?>
        <h4>Statistiques d'occupation</h4>
        
        Statistiques d'occupation globale : <?php echo $data['occupation_globale'] ?> %. <br>
        <!-- Récupérer via variable en parametre -->

        <?php 
            foreach($data["occupation_par_residence"] as $index):
        ?>
        Occupation de la résidence nommée <?php  echo $index['residence']?> : <?php echo $index['percentage'] ?> %. <br>
        <?php endforeach; ?>
        </div>  
    </body>
</html>