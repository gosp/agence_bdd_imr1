﻿<html>
    <header>
        <title>Connexion</title>
        <link rel="stylesheet" type="text/css" href="style/main.css" />
    </header>

    <body>
    <div id="content">        
        <h4>Connexion</h4>

        <?php if(isset($data['error_message']) && $data['error_message'] != "") : ?>
            <h5><?php echo $data['error_message']; ?></h5>
        <?php endif; ?>
        <?php if(isset($data['success_message']) && $data['success_message'] != "") : ?>
            <h5><?php echo $data['success_message']; ?></h5>
        <?php endif; ?>

           <!-- formulaire de connexion -->
           <form method="post" action="index.php?page=connection&action=connect">
            <table>
                <tr>
                    <td><label for="idpersonne">Identifiant : </label></td>
                    <td><input type="text" id="idpersonne" name="idpersonne"></td>
                </tr>
                <tr>
                    <td><label for="nom">Nom : </label></td>
                    <td><input type="text" id="nom" name="nom"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="Valider"></td>
                </tr>
            </table>
       </form>

       <h4>Inscription</h4>
       <!-- formulaire d'inscription -->
       <form method="POST" action="index.php?page=connection&action=sign">
            <table>
                <tr>
                    <td>
                        <label for="nom">Nom : </label>
                    </td><td>
                        <input type="text" id="nom" name="nom">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="prenom">Prenom : </label>
                    </td><td>
                        <input type="text" id="prenom" name="prenom">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" value="Valider">
                    </td>
                </tr>
            </table>
       </form>
    </div>
    </body>
</html>