﻿<html>
    <header>
        <title>Liberation logement</title>
        <link rel="stylesheet" type="text/css" href="style/main.css"/>
    </header>
    <body>
        <div id="content">             
        <?php require_once("menu.php"); ?>
        <h4>Liberation d'un logement</h4>
           <!-- formulaire de choix du locataire -->
        <form method="POST" action="index.php?page=liberation">
            <table>
                <tr>
                    <td>
                        <label for="idlocataire">Numero du locataire : </label>
                    </td><td>
                        <input type="text" id="idLocataire" name="idlocataire">
                    </td><td>
                </tr><tr>
                    <td></td><td>
                        <input type="submit" value="Rechercher">
                    </td>
                </tr>
            </table>
        </form>
        <br>
        <?php if(isset($data["infos_personne"])): ?>
        <?php if(!empty($data["infos_personne"])): ?>
        <form method="POST" action="index.php?page=liberation&action=liberate">
            <table>
                <tr>
                    <td>
                        Nom :
                    </td><td>
                        <?php echo $data["infos_personne"]["nom"];?>
                    </td><td>
                </tr><tr>
                    <td>
                        Prenom :
                    </td><td>
                        <?php echo $data["infos_personne"]["prenom"];?>
                    </td>
                </tr><tr>
                    <td></td><td>
                        <input type="hidden" name="idSupprime" value="<?php echo $data['infos_personne']['idpersonne'];?>">
                    </td><td>
                        <input type="submit" value="Liberer">
                    </td>
                </tr>
            </table>
        </form>
        <?php else: ?>
        Aucun locataire ne porte ce numéro.   
        <?php endif; ?> 
    <?php endif;?>
    </div>
    </body>
</html>