﻿<html>
    <header>
       <title>Accueil</title>
       <link rel="stylesheet" type="text/css" href="style/main.css" />
    </header>
    <body>        
    <div id="content">
        <?php require_once("menu.php"); ?>
        <h4>Accueil</h4>

        <!-- Information profil -->
        Bonjour <?php echo $data['prenom']. ' ' .$data['nom']."."; ?>
        <br>
        Votre identifiant est : <?php echo $data['identifiant']."."; ?>
        <br><br>

        <!-- Information locataire ou non -->
        <?php if ($data['location']) 
        {
            echo "Vous êtes actuellement locataire de l'appartement ". $data['location']['id_appartement']." de type ".$data['location']['nom_type']." dans la résidence ".$data['location']['nom_residence']." avec un loyer mensuel de : ".$data['location']['loyer']. "€.";
        } 
        else
        {
            echo "Vous ne possédez pas d'appartement.";
        }?>
        <br>

        <!-- Information nombre demandes -->
        <?php if($data['demandes'] <= 0)
        {
            echo "Vous n'avez aucune demande en attente.";
        }
        else if($data['demandes'] == 1)
        {
            echo "Vous avez 1 demande en attente.";
        } 
        else
        {
            echo "Vous avez ".$data['demandes']. " demandes en attente.";
        }?>

        <br><br><hr><br>

        Vous pouvez accéder à la liste de vos demandes <a href="index.php?page=demand">ici</a>.
    </div>
    </body>
</html>