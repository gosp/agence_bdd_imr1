﻿<html>
    <header>
       <title>Liste d'attente</title>
       <link rel="stylesheet" type="text/css" href="style/main.css" />
    </header>
    <body>     
    <div id="content">   
        <?php require_once("menu.php"); ?>
        <h4>Liste d'attente</h4>

           <!-- formulaire de liste d'attente -->
           <form method="POST" action="index.php?page=waitingList&action=search">
            <table>
                <tr>
                    <td>
                        <label for="type">Type d'appartement :</label>
                    </td>
                    <td>
                        <select name="type">
                            <?php foreach ($data['types'] as $type): ?>
                                <option value="<?php echo $type['nom']; ?>"><?php echo $type['nom']; ?></option>
                            <?php endforeach ?>

                        </select>
                    </td>
                </tr><tr>
                    <td></td><td>
                        <input type="submit" value="Afficher">
                    </td>
                </tr>
            </table>
       </form>

        <?php
            if(isset($_POST["type"])):
                echo "<h4>Affichage des appartements de type ".$_POST["type"]."</h4>";
        ?>
            <table border="1" style="border-collapse:collapse;">
                <tr>
                    <td>Type</td>
                    <td>Résidence</td>
                    <td>Nom</td>
                    <td>Prénom</td>
                    <td>Date demande</td>
                </tr>
                <?php foreach($data['demandes'] as $demande): ?>
                    <tr>
                        <td><?php echo $demande['nom_type']; ?></td>
                        <td><?php echo $demande['nom_residence']; ?></td>
                        <td><?php echo $demande['nom']; ?></td>
                        <td><?php echo $demande['prenom']; ?></td>
                        <td><?php echo $demande['date_demande']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif;?>
    </body>
</html>